# Completion for avantigen

To enable tasks auto-completion in shell you should add `eval "$(avantigen --completion=shell)"` in your `.shellrc` file.

## Bash

Add `eval "$(avantigen --completion=bash)"` to `~/.bashrc`.

## Zsh

Add `eval "$(avantigen --completion=zsh)"` to `~/.zshrc`.