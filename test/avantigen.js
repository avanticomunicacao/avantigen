'use strict';
var spawn = require('child_process').spawn,
    path = require('path');

describe('avantigen', function () {
  it('should list installed generators', function (done) {
    var avantigen = runAvantigen();
    var data = '';
    avantigen.stdout.on('data', function (chunk) {
      data += chunk;
    });
    avantigen.on('close', function (code) {
      code.should.equal(0);
      data.should.match(/\[avantigen\] ├── bad/);
      data.should.match(/\[avantigen\] └── test/);
      done();
    });
  });

  it('should list tasks for given generator', function (done) {
    var avantigen = runAvantigen(['test', '--tasks']);
    var data = '';
    avantigen.stdout.on('data', function (chunk) {
      data += chunk;
    });
    avantigen.on('close', function (code) {
      code.should.equal(0);
      data.should.match(/├── default/);
      data.should.match(/└── app/);
      done();
    });
  });

  it('should run `default` task in generator, when task is not provided', function (done) {
    var avantigen = runAvantigen(['test']);
    var data = '';
    avantigen.stdout.on('data', function (chunk) {
      data += chunk;
    });
    avantigen.on('close', function (code) {
      code.should.equal(0);
      data.should.match(/\ndefault\n/);
      done();
    });
  });

  it('should run provided task in generator', function (done) {
    var avantigen = runAvantigen(['test:app']);
    var data = '';
    avantigen.stdout.on('data', function (chunk) {
      data += chunk;
    });
    avantigen.on('close', function (code) {
      code.should.equal(0);
      data.should.match(/\napp\n/);
      done();
    });
  });

  it('should run provided task with arguments in generator', function (done) {
    var avantigen = runAvantigen(['test:app', 'arg1', 'arg2']);
    var data = '';
    avantigen.stdout.on('data', function (chunk) {
      data += chunk;
    });
    avantigen.on('close', function (code) {
      code.should.equal(0);
      data.should.match(/\napp \(arg1, arg2\)\n/);
      done();
    });
  });

  it('should fail when running a non-existing task in a generator', function (done) {
    var avantigen = runAvantigen(['test:noexist']);
    var data = '';
    avantigen.stdout.on('data', function (chunk) {
      data += chunk;
    });
    avantigen.on('close', function (code) {
      code.should.equal(1);
      data.should.match(/\[avantigen\] Task 'noexist' was not defined in `avantigen-test`/);
      done();
    });
  });

  it('should fail when running a generator without avantigenfile', function (done) {
    var avantigen = runAvantigen(['bad']);
    var data = '';
    avantigen.stdout.on('data', function (chunk) {
      data += chunk;
    });
    avantigen.on('close', function (code) {
      code.should.equal(1);
      data.should.match(/\[avantigen\] No avantigenfile found/);
      data.should.match(/\[avantigen\].+issue with.+`avantigen-bad`/);
      done();
    });
  });

  it('should fail trying to run a non-existing generator', function (done) {
    var avantigen = runAvantigen(['noexist']);
    var data = '';
    avantigen.stdout.on('data', function (chunk) {
      data += chunk;
    });
    avantigen.on('close', function (code) {
      code.should.equal(1);
      data.should.match(/\[avantigen\] No generator by name: "noexist" was found/);
      done();
    });
  });
});

function runAvantigen (args) {
  args = args || [];
  var avantigen = spawn('node', [path.join(__dirname, '..', 'bin', 'avantigen.js')].concat(args), {cwd: __dirname});
  avantigen.stdout.setEncoding('utf8');
  return avantigen;
}
