# AVANTI Gen
> The streaming scaffolding system to Avanti agency. 

Avantigen is a tool to generate starter files to our projects.

Avantigen does not contain anything "out of the box", except the ability to locate installed generators
and to run them with [`liftoff`](https://www.npmjs.org/package/liftoff).

## Install

Install `avantigen` globally with:

```bash
npm install -g avantigen
```

## Usage

```bash
avantigen <generator>[:<tasks>] [<args>]
```

* `<tasks>`: a colon (":") separated list of a task or tasks to run. If not provided the `default` task in the avantigenfile is run
* `<args>`: any other given arguments (not prefixed with "--" or "-") can be accessed via the `gulp.args` property from within a avantigenfile

**Example:**

```bash
avantigen react:component myNewComponent
```

### List available generators

If run without any arguments, Avantigen will list all installed generators.

```bash
avantigen
```

### List available tasks in generator

To list available tasks within a generator, use the generator name in conjunction with the `--tasks` parameter.

```bash
avantigen <generator> --tasks
```

### Print version(s)

As usual you can use `-v` or `--version` to get the current Avantigen version:

```bash
avantigen -v
```

It can also be used together with a generator name:

```bash
avantigen <generator> -v
```

You'll then get the version for avantigen, the gulp version installed in the generator and the version number of the given generator.

## Documentation

### Things to remember

**When using Avantigen globally:**

* Install `avantigen` globally
* Install avantigen generators globally

**When using Avantigen locally:**

* Install `avantigen` locally
* Install avantigen generators locally
* Preferably add `"avantigen": "avantigen"` to the `"scripts"` section in your `package.json` and run avantigen like so: `npm run avantigen`

### Avantigen uses gulp

Avantigen is just the global excutable to trigger avantigen generators, under the hood it's still gulp that is run using each avantigenfile as config file.

### The avantigenfile

A avantigenfile is basically a gulpfile, but meant to be used to scaffold project structures.

#### Why not name it "gulpfile" then?

Because a Avantigen generator may want to use gulp locally for linting, testing and other purposes, in which case it will need to have a gulpfile.

#### Sample avantigenfile

Given a avantigen generator project structure with a web app project template inside `./templates/app/`, a avantigenfile could be designed like this:

```javascript
var gulp = require('gulp'),
    install = require('gulp-install'),
    conflict = require('gulp-conflict'),
    template = require('gulp-template'),
    inquirer = require('inquirer');

gulp.task('default', function (done) {
  inquirer.prompt([
    {type: 'input', name: 'name', message: 'Give your app a name', default: gulp.args.join(' ')}, // Get app name from arguments by default
    {type: 'confirm', name: 'moveon', message: 'Continue?'}
  ],
  function (answers) {
    if (!answers.moveon) {
      return done();
    }
    gulp.src(__dirname + '/templates/app/**')  // Note use of __dirname to be relative to generator
      .pipe(template(answers))                 // Lodash template support
      .pipe(conflict('./'))                    // Confirms overwrites on file conflicts
      .pipe(gulp.dest('./'))                   // Without __dirname here = relative to cwd
      .pipe(install())                         // Run `bower install` and/or `npm install` if necessary
      .on('end', function () {
        done();                                // Finished!
      })
      .resume();
  });
});
```

## Want to contribute?

Anyone can help make this project better!
